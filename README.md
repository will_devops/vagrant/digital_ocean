# Digital Ocean Vagrant Template

A template to set up virtual machines on Digital Ocean

# Usage

## Installing vagrant

To install vagrannt, first find the appropriate package for your system and download it.

    https://www.vagrantup.com/downloads.html

If you're using Homebrew

    $ brew install vagrant

## Configuring Files

Create a file named **.do_token** with your Digital Ocean `TOKEN`, more information in the link below

    https://www.digitalocean.com/docs/api/create-personal-access-token/

You can manage your settings in **config.yml** file.

## Running vagrant

    $ git clone https://gitlab.com/will_devops/vagrant/digital_ocean
    $ cd digital_ocean
    $ vagrant up

## Supported versions

This templates was tested using a vagrant 2.2.5.
